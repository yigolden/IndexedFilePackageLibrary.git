﻿using System.Runtime.CompilerServices;

namespace IndexedFilePackageLibrary
{
    class Box<T> where T : struct
    {
        private T _value;

        public Box(T value)
        {
            _value = value;
        }
        
        public ref T Item
        {
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            get
            {
                return ref _value;
            }
        }

        public static implicit operator Box<T>(T value) => new Box<T>(value);
        public static implicit operator T(Box<T> box) => box.Item;

    }
}
