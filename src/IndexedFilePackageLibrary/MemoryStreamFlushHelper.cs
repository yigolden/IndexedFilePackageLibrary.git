﻿using System.Threading.Tasks;

namespace System.IO
{
    static class MemoryStreamFlushHelper
    {
        public static void FlushTo(this MemoryStream ms, Stream stream)
        {
            ms.Position = 0;
            ms.CopyTo(stream);
            ms.Position = 0;
            ms.SetLength(0);
        }

        public static async Task FlushToAsync(this MemoryStream ms, Stream stream)
        {
            ms.Position = 0;
            await ms.CopyToAsync(stream).ConfigureAwait(false);
            ms.Position = 0;
            ms.SetLength(0);
        }
    }
}
